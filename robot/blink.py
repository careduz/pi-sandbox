from time import sleep

from gpiozero import LED
from utils import logger

logger = logger.get(__name__)


def start(pin: int):
    led = LED(pin)
    logger.debug("Kicking off...")
    while True:
        try:
            led.on()
            sleep(1)
            led.off()
            sleep(1)
        except KeyboardInterrupt:
            logger.info("Finishing")
            break
    return


if __name__ == "__main__":
    start(17)
