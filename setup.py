from setuptools import find_packages, setup

# -*- coding: utf-8 -*-

with open("README.md") as f:
    readme = f.read()

with open("LICENSE") as f:
    license = f.read()

setup(
    name="robot",
    version="0.1",
    description="Raspberry Pi car project",
    url="https://gitlab.com/careduz/pi-sandbox",
    author="Carlos Hernandez",
    author_email="carlos@uncork.ai",
    long_description=readme,
    license=license,
    packages=find_packages(exclude=("tests", "docs")),
)
