import logging
import sys

logging.basicConfig(
    level=logging.DEBUG,
    stream=sys.stdout,
    format="%(asctime)s | %(name)s | %(levelname)s | %(message)s",
)
loggers = {}


def get(name: str) -> logging.Logger:
    print(f"GET! {name}")
    global loggers

    if loggers.get(name):
        return loggers[name]

    logger = logging.getLogger(name)

    # f =
    # handler = logging.StreamHandler(sys.stdout)
    # handler.setLevel(logging.DEBUG)
    # handler.setFormatter(logging.Formatter(f))
    # logger.addHandler(handler)

    loggers[name] = logger

    return loggers[name]
